/**
 * Stub for Node class
 * 
 * @author CS Staff
 * @param <T> the generic type; extends Comparable
 */

public class Node<T extends Comparable<T>> {
 
    /**
     * Instantiates a new node.
     *
     * @param value
     *            the value
     */
    public Node(T value) {

    }
}
